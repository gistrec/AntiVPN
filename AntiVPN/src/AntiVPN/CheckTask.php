<?php

namespace AntiVPN;

use pocketmine\scheduler\AsyncTask;
use pocketmine\Server;
use pocketmine\Player;

class CheckTask extends AsyncTask{

    private $status = null;
    private $errorMsg = null;
    private $closeMsg;
    private $version;
    private $player;

    public function __construct(Player $player, $version, $closeMsg){
        $this->player = $player;
        $this->version = $version;
        $this->closeMsg = $closeMsg;
    }

    public function onRun(){
        $query = "http://checkvpn.ru/api/" . $this->player->getAddress() . "+" . $this->version;
        @$status = file_get_contents($query);
        if ($status != null){
            $status = json_decode($status);
            if (isset($status->{"error-msg"})) $this->errorMsg = $status->{"error-msg"};
            if (isset($status->{"status"}) && isset($status->{"host-ip"})){
                if ($status->{"status"} == "success" && $status->{"host-ip"} == "true") $this->status = true;
                else $this->status = false;
            }
        }
    }
    
    public function onCompletion(Server $server){
        if ($this->errorMsg != null) $server->getLogger->info($this->errorMsg);
        if ($this->status && $this->player->isOnline()){
            $server->getLogger()->info("§cВнимание, §b".$this->player->getName()."§c попытался зайти с прокси-ip §b".$this->ip);
            $this->player->close("", $this->closeMsg);
        }
    }
}