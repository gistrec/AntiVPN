<?php

namespace AntiVPN;

use pocketmine\plugin\PluginBase;
use pocketmine\event\Listener;
use pocketmine\event\player\PlayerPreLoginEvent;
use pocketmine\command\CommandSender;
use pocketmine\command\Command;
use pocketmine\utils\Config;

class AntiVPN extends PluginBase implements Listener{

    public $version = 3;

    public $config;
    public $whiteList;

    public function onEnable(){
        @mkdir($this->getDataFolder());
        if (!file_exists($this->getDataFolder().'config.yml')) $this->createConfig();
        if (!file_exists($this->getDataFolder().'whiteList.yml')) $this->createWhiteList();
        $this->whiteList = (new Config($this->getDataFolder().'whiteList.yml', Config::YAML))->getAll();
        $this->config = (new Config($this->getDataFolder().'config.yml', Config::YAML))->getAll();
        $this->getServer()->getPluginManager()->registerEvents($this, $this);
        $this->getLogger()->info("§bAntiVPN §aby §bgistrec [vk.com/AreaOfDefect] §aзагружен!");
    }

    public function onConnect(PlayerPreLoginEvent $event){
        if ($this->config['enable'] == true){
            if (!isset($this->whiteList[strtolower($event->getPlayer()->getName())])){
                $task = new CheckTask($event->getPlayer(), $this->version, $this->config['msg']);
                $this->getServer()->getScheduler()->scheduleAsyncTask($task);
            }
        }
    }

    public function onCommand(CommandSender $sender, Command $command, $label, array $args){
        if (strtolower($command->getName()) == 'av'){
            if (!isset($args[0])) return ($sender->sendMessage("§bИспользуйте /av help"));
            switch (strtolower($args[0])){
                case 'disable':
                case 'off':
                    if ($this->config['enable'] == true){
                        $this->config['enable'] = false;
                        $sender->sendMessage("§aAntiVPN отключен. §cВнимание, серверу может угрожать опасность!");
                    }else {
                        $sender->sendMessage("§aAntiVPN уже отключен. §cВнимание, серверу может угрожать опасность!");
                    }
                    break;
                case 'enable':
                case 'on':
                    if ($this->config['enable'] == false){
                        $this->config['enable'] = true;
                        $sender->sendMessage("§aAntiVPN включен.");
                    }else {
                        $sender->sendMessage("§aAntiVPN уже включен.");
                    }
                    break;
                case 'whitelist':
                case 'wl':
                    if (!isset($args[1]) || !isset($args[2])) return ($sender->sendMessage("§bИспользуйте /av ".$args[1]." <add/remove> <player>"));
                    switch (strtolower($args[1])) {
                        case 'add':
                            if (isset($this->whiteList[strtolower($args[2])])) {
                                $sender->sendMessage("§aИгрок уже находится в white list!");
                            }else {
                                $this->whiteList[strtolower($args[2])] = true;
                                $sender->sendMessage("§aИгрок успешно добавлен в white list!");
                            }
                            break;
                        case 'remove':
                            if (!isset($this->whiteList[strtolower($args[2])])) {
                                $sender->sendMessage("§aИгрока нет в white list!");
                            }else {
                                unset($this->whiteList[strtolower($args[2])]);
                                $sender->sendMessage("§aИгрок успешно убран из white list!");
                            }
                            break;
                        default:
                            $sender->sendMessage("§bИспользуйте /av ".$args[1]." <add/remove> <player>");
                            break;
                    }
                    break;
                case 'info':
                    $sender->sendMessage("§aПлагин §bAntiVPN §aнеобходим, чтобы игроки не могли заходить с прокси ip.\n§aАвтор плагина: §bgistrec [vk.com/AreaOfDefect]");
                    break;
                default:
                    $sender->sendMessage("§aДоступные команды:\n§bav on/off §aвключение/выключение плагина\n§bav wl §aуправление исключениями");
                    break;
            }
        }
    }

    public function onDisable() {
        $config = new Config($this->getDataFolder().'config.yml', Config::YAML);
        $config->setAll($this->config);
        $config->save();
        $whiteList = new Config($this->getDataFolder().'whiteList.yml', Config::YAML);
        $whiteList->setAll($this->whiteList);
        $whiteList->save();
    }

    public function createConfig(){
        file_put_contents($this->getDataFolder().'config.yml', 
"#Включен ли плагин
enable: true
#Сообщение, выводимое при кике
msg: '§cНа сервер нельзя заходить с прокси серверов!'");
    }

    public function createWhiteList(){
        file_put_contents($this->getDataFolder().'whiteList.yml', 
"#Здесь будут ники, которым разрешено заходить с любых IP
name: true");
    }
}